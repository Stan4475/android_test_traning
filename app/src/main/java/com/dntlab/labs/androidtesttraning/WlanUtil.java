package com.dntlab.labs.androidtesttraning;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import de.rtner.misc.BinTools;

/**
 * Created by jenkins on 2/24/17.
 */

public class WlanUtil {
    public static final Pattern JUICENET_WLAN_NAME_PATTERN = Pattern.compile("Juice([Bb]ox|[Nn]et)-[0-9a-zA-Z]{3}");
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

    public static String encodeWithPBKDF2(String ssid, String password) {

        String result;

        byte[] salt = ssid.getBytes(Charset.forName("UTF-8"));
        byte[] dk = new byte[0];
        try {
            dk = pbkdf2(password.toCharArray(), salt, 4096, 32);
        } catch (Exception e) {
            //never throws
            return password;
        }
        result = BinTools.bin2hex(dk);
        return result.toLowerCase();
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return skf.generateSecret(spec).getEncoded();
    }

    public static boolean isJuiceNetWLAN(String SSID){
        return JUICENET_WLAN_NAME_PATTERN.matcher(SSID).matches();
    }
}
